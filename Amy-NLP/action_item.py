# -*- coding: utf-8 -*-
"""
Created on Sun Dec 09 12:53:29 2018

@author: Sachin Sharma
"""

import os, json

import sentiment

java_path = "C:/Program Files/Java/jdk1.8.0_191/jre/bin/java.exe"
os.environ['JAVAHOME'] = java_path

fl = "data2.txt"

text = '''
we are here t discuss important points regarding the orc project. 
vijay will be looking after the environment issues.
lets setup the enviromnent fast.
complete the ui interface by tomorrow.
follow the big data team for hive issue.
combine the sources to create big data lake.
please look into this
'''


text2 = '''
we completed code 1 yesterday.
we are doing code2 now.
lets try and complete code3 by tomorrow.
complete the work today.
vijay will work on the code4.
vijay has done code4.
please do this task.
kindly do this task5.
i will work on task6.
complete task7 and then we can start with task8.
task9 needs to be completed by eod.
task10 was completed yesterday by changing the configuration.
he will do the task10.
just complete the task11 and lets see this tomorrow
'''

from nltk import pos_tag
from nltk.tag.stanford import StanfordPOSTagger as POS_Tag

#c=english_postagger.tag(text)
#print(c)

action_keywords = ['please','kindly']

'''
for t in text2.split('.'):
    print("NLTK")
    print(pos_tag(t.split()))
    print("STANFORD")
    print(english_postagger.tag(t.split()))
    print("NEXT")
'''    

from nltk import RegexpParser
from nltk.tree import Tree

def is_imperative(tagged_sent):
    
    
    if tagged_sent[0][1] == "VB" or tagged_sent[0][1] == "MD":
        return True

    else:
        chunk = get_chunks(tagged_sent)
        if type(chunk[0]) is Tree and chunk[0].label() == "VB-Phrase":
            return True

    

    return False

def get_chunks(tagged_sent):
    chunkgram = r"""VB-Phrase: {<DT><,>*<VB>}
                    VB-Phrase: {<VBP><TO>}
                    VB-Phrase: {<RB><VB>}
                    VB-Phrase: {<UH><,>*<VB>}
                    VB-Phrase: {<UH><,><VBP>}
                    VB-Phrase: {<PRP><MD>*<VB>}
                    VB-Phrase: {<NN.?>+<,>*<MD>*<VB>}
                    """
    chunkparser = RegexpParser(chunkgram)
    return chunkparser.parse(tagged_sent)

text = '''How are you, how are you hear me thanks for joining this call? We are testing, hear any application which is supposed to use voice, detection water. Then then Tamil natural language processing and generate some am. I am interested in. You go on strongly define photos and get me have all the requirements gathering time you think requirement. They are gathering in the Dell fashion and you started with requirement and call me to go keyboard. I want to see in cloud, but you are right. I think the action items on me. I will think that and include that, in my sprint planning write an email if you can have action on BST, met Sejal how much time it take to complete. I would need some information from you for before that. I ain't getting the requirement on that.. If you could give me that. This friday, I would be able to submit the estimation by sahli's Monday. Ok, let's, let's get together on 17th December corporate plan. okay
'''


'''
for t in text.split('.'):
    print(t)
    print(english_postagger.tag(t.split()))
    print(is_imperative(english_postagger.tag(t.split())))
'''

def get_action_items(id,text):
    print("Called S")
    
    with open(fl) as f:
        data = json.load(f)
    f.close()
    
    print(data)
    
    english_postagger = POS_Tag('models/english-bidirectional-distsim.tagger', 'stanford-postagger.jar')

    #text = data[id]['text']
    
    action_item = []
    for t in text.split('.'):
        print(t, english_postagger.tag(t.split()))
        if t:
            if is_imperative(english_postagger.tag(t.split())):
                
                action_item.append({'action':t,'name':'User'}) 
    
    data[str(id)]['act_pt'] = action_item
    #print(data[id])
    
    with open(fl,'w') as f:
        f.write(json.dumps(data, indent=3))
    f.close()
    
    sentiment.get_sentiment_score(id, text)
    
    
  

def get_action_items_n(text):
    print("Called N")
    
    act_item = ''
    for t in text.split('.'):
        if is_imperative(pos_tag(t.split())):
            act_item = act_item + '.' + t
    return act_item

#text = '''
#Lets have a quick catch for onboarding planning. We have decided to hire 5 people for ORC project. We have given opening in weekly IJP notifications. We havent received any candidates. So far, do we have any plans to hire from contractors? We are in touch with few contactors. We have shared the job descriptions with them. They are yet to get back to us. I will ask HR is they can help in esclaing out matter. Lets catch up again on Thursday'''
#print(get_action_items(1,text))
#print(get_action_items_n(text))