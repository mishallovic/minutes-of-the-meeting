# -*- coding: utf-8 -*-
"""
Created on Sun Dec 09 00:40:40 2018

@author: Sachin Sharma
"""

import json
import keywords
    
from nltk.sentiment.vader import SentimentIntensityAnalyzer
sid = SentimentIntensityAnalyzer()

fl = "data2.txt"


def get_sentiment_score(id, text):
    print("Sentiment")
    with open(fl) as f:
        data = json.load(f)
    f.close()
    
    text = data[id]['text']
    
    PI = sid.polarity_scores(text)['compound'] 

    data[str(id)]['PI'] = PI
    
    print(data)
    
    with open(fl,'w') as f:
        f.write(json.dumps(data, indent=3))
    f.close()
    
    keywords.get_keywords(id, text)
    

