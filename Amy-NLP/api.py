# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 16:45:01 2018

@author: Sachin Sharma
"""

import requests
from flask import request

from flask import Flask
from flask_cors import CORS


import meeting_list_api
import meeting_by_id
import text_summ
import get_all
import save_meeting
import jira_api


import os 


import matplotlib.pyplot as plt 
from os import path 
from wordcloud import WordCloud 


from flask import send_file 

app = Flask(__name__)


CORS(app)


@app.route('/meetings') 
def get_meetings():
    return meeting_list_api.get_meetings() 

@app.route('/meeting/save', methods=['POST']) 
def save_meetings():    
    content = request.json
    
    id = content['id']
    name = content['meeting_name']
    MoM = content['MoM']
    action_points = content['actionItems']
    
    save_meeting.save_meet(id, MoM, action_points, name)
    jira_api.assign_jira(id,action_points)
    
    
    
    return 'True'


@app.route('/nlp') 
def transform_text():
    
    id = request.args.get('id')
    text = request.args.get('text')
    ratio = request.args.get('ratio', default = 0.4)
    flag = request.args.get('flag', default = 'True')
    length = request.args.get('length', default = 0)
    
    
    return text_summ.get_summary(id, text, float(ratio), flag, length)

@app.route('/meeting_by_id')     
def get_meeting_by_id():
    id = request.args.get('id')
    return meeting_by_id.get_meeting_by_id(id)

@app.route('/get_all')     
def get_all_meetings():    
    return get_all.get_all_meetings()

@app.route('/wordcloud' +'/<text>', methods=['GET']) 
def showForm(text): 
    #return HTML.format(APP_NAME=os.getenv("APP_NAME", "Unknown"), APP_PORT=os.getenv("APP_PORT", "Unknown"), APP_DATA=os.getenv("APP_DATA", "Unknown"), TEMP=os.getenv("TEMP","Unknown"), PYVER=platform.python_version()) 
    d = path.dirname(_file) if "file_" in locals() else os.getcwd() 
    # Read the whole text. 
    #text = open(path.join(d, 'constitution.txt')).read() 
    # Generate a word cloud image 
    wordcloud = WordCloud().generate(text) 
    plt.imshow(wordcloud, interpolation='bilinear') 
    plt.axis("off") 
    # lower max_font_size 
    wordcloud = WordCloud(max_font_size=40).generate(text) 
    plt.figure() 
    plt.imshow(wordcloud, interpolation="bilinear") 
    plt.axis("off")
    #plt.show() 
    plt.savefig('testplot.jpeg') 
    img= open('testplot.jpeg') 
    type(img) 
    return send_file( 'testplot.jpeg', mimetype='image/jpg' ) 

if __name__ == "__main__":
    app.run(host="0.0.0.0", port="9000")