import requests

url = "http://api.sendgrid.com/v3/mail/send"

payload = "{\"personalizations\":\r\n        [{\"to\":[{\"email\":\"sachin.sharma0728@gmail.com\",\"name\":\"Punyam\"}],\r\n        \"subject\":\"Here is your meeting summary:\"}],'\r\n        \"from\":{\"email\":\"nitesh.singh@hsbc.co.in\",\"name\":\"Amy\"},\r\n        \"reply_to\":{\"email\":\"nitesh.singh@hsbc.co.in\",\"name\":\"Amy\"},\r\n        \"subject\":\"Here is the meeting summary\",\r\n        \"content\":[{\"type\":\"text/html\",\r\n        \"value\":\"Hi\"}]}"
headers = {
    'authorization': "Bearer ",
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "b4f37651-fc56-a193-05d4-88240f0121c6"
    }

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)