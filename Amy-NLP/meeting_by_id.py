# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 17:35:24 2018

@author: Sachin Sharma
"""


import json

fl = "data2.txt"

def get_meeting_by_id(id):     
    with open(fl) as f:
        data = json.load(f)
               
    return json.dumps(data[id])
 
   