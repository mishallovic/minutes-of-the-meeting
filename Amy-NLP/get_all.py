# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 18:02:36 2018

@author: Sachin Sharma
"""

import json

fl = "data2.txt"



def get_all_meetings():     
    with open(fl) as f:
        data = json.load(f)
               
    return json.dumps(data, indent = 3)