# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 15:37:48 2018

@author: Sachin Sharma
"""

import json
import datetime


fl = "data2.txt"


def get_meetings():
    meetings = []
    
    with open(fl) as f:
        data = json.load(f)
    date = datetime.datetime.now()
    
    for k,v in data.items():
        meet = {}
        meet['meeting_id'] = k
        meet['subject'] = "Subject"
        meet['meeting_date'] = str(date.day) + "-" + str(date.month) + "-" + str(date.year)
        meet['prdctvity_index'] = v['PI']
        meet['keyword'] = v['Keyword']
        meet['text'] = v['text']
        meet['next_meet'] = v['next_meet']
        meet['act_pt'] = v['act_pt']
        meet['edit'] = v['edit']
        meet['length'] = v['length']
        meet['name'] = v['name']
        meetings.append(meet)
        
    return json.dumps(meetings)
 
     