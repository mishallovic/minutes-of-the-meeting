
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 15:37:48 2018

@author: Sachin Sharma
"""

import json

fl = "data2.txt"

def save_meet(id, MoM, act_pts, name):
    
    with open(fl) as f:
        data = json.load(f)
        
    
    data[id]['MoM'] = MoM
    data[id]['act_pt'] = act_pts
    data[id]['name'] = name
    data[id]['edit'] = False
    
    with open(fl,'w') as f:
        f.write(json.dumps(data, indent=3))
    f.close()
 
     