# -*- coding: utf-8 -*-
"""
Created on Sun Dec 09 19:20:21 2018

@author: Sachin Sharma
"""

import sentiment
import text_summ
import action_item
import next_meet


text = ''
with open("C:\\Users\\Sachin Sharma\\Desktop\\minutes2.txt") as f:
    for line in f:
        text = text + line.replace('\n','').replace('?','.')

print("Sentiment Score")
print(sentiment.get_sentiment_score(text))

print('Summary')
print(text_summ.get_summary(text, 0.05))

#print('Keywords')
#print(text_summ.get_keywords(text))

print('Action Items')
print(action_item.get_action_items(text))

print('Suggeted Meetings')
print(next_meet.extract_dates(text))
        
        
