# -*- coding: utf-8 -*-
"""
Created on Sun Dec  9 20:56:32 2018

@author: nitesh
"""

import requests
import json
import pickle 
import urllib.request
import threading

def getMp3():
    url = "https://www.freeconferencecall.com/v2/conferences"
    meeting_id = {}
    new_meeting_id = {}
    try:
        meeting_id = pickle.load(open('meeting_id.pkl', 'rb') )
    except (OSError, IOError) as e:
        meeting_id = {}
    querystring = {"start_time":"","end_time":"","has_recordings":"false","offset":"0","limit":"10","order_by":"start_time","order":"DESC","description":"","client_offset":"330","abandoned_calls":"false","_":"1544349066295"}
    
    headers = {
        'accept': "application/json, text/javascript, */*; q=0.01",
        'dnt': "1",
        'x-csrf-token': "wd6I9Vpe2zTd3m7EwYeCyidwV1DF5nDRanTg7CgxSWt8JqlvBky64uLd0DNx9kw9+a2kM1Tw+m2m5UTpJErVfw==",
        'x-requested-with': "XMLHttpRequest",
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36",
        'referer': "https://www.freeconferencecall.com/profile",
        'accept-encoding': "gzip, deflate, br",
        'accept-language': "en-IN,en;q=0.9,hi-IN;q=0.8,hi;q=0.7,en-GB;q=0.6,en-US;q=0.5",
        'cookie': "country_code=in; _fcc_plain_session=9bcbf4a0b42f4b097f0b0f3f0bdbdcd0; _fccrm=BAh7BjoLcGNzX2lkSUM6H05vcmk6OlN0cmluZ1dpdGhBdHRyaWJ1dGVzIg0xMDQ0Mzc3Mwc6BkVUOhBAYXR0cmlidXRlc3sA--f05c930091e222427cb2cebbe2879ee6849de5a7; locale=en; ga_new_or_upgraded_user_koolestnitz=1; _ga=GA1.2.1681402444.1544348579; _gid=GA1.2.627008198.1544348579",
        'cache-control': "no-cache",
        'postman-token': "12fdf779-c46c-529f-551c-ff95e6b7f5fa"
        }
    
    response = requests.request("GET", url, headers=headers, params=querystring)
    
    json_encoded = json.loads(response.text)
    for value in json_encoded['conferences']:
        id = value['id']
        print (id)
        if value['recording_duration'] > 1:
            if id not in meeting_id:
                new_meeting_id[id] = "url"
                meeting_id[id] = "url"
            else:
                print("old meeting")
        else:
            print("no a recorded call, ignoring")
    for id in new_meeting_id:
        url = "https://www.freeconferencecall.com/v2/conferences/"+id+"/recordings"
        
        headers = {
        'accept': "application/json, text/javascript, */*; q=0.01",
        'dnt': "1",
        'x-csrf-token': "SF1eI5GMxaYtcTODZNDP/9NBXwbvpTk4ufOULRIeKOj1pX+5zZ6kcBJyjXTUoQEIDZysZX6zs4R1YjAoHmW0/A==",
        'x-requested-with': "XMLHttpRequest",
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36",
        'referer': "https://www.freeconferencecall.com/wall/koolestnitz/host/history?order_by=reference_number",
        'accept-encoding': "gzip, deflate, br",
        'accept-language': "en-IN,en;q=0.9,hi-IN;q=0.8,hi;q=0.7,en-GB;q=0.6,en-US;q=0.5",
        'cookie': "country_code=in; _fcc_plain_session=9bcbf4a0b42f4b097f0b0f3f0bdbdcd0; _fccrm=BAh7BjoLcGNzX2lkSUM6H05vcmk6OlN0cmluZ1dpdGhBdHRyaWJ1dGVzIg0xMDQ0Mzc3Mwc6BkVUOhBAYXR0cmlidXRlc3sA--f05c930091e222427cb2cebbe2879ee6849de5a7; locale=en; ga_new_or_upgraded_user_koolestnitz=1; _ga=GA1.2.1681402444.1544348579; _gid=GA1.2.627008198.1544348579; country_code=in; _fcc_plain_session=9bcbf4a0b42f4b097f0b0f3f0bdbdcd0; _fccrm=BAh7BjoLcGNzX2lkSUM6H05vcmk6OlN0cmluZ1dpdGhBdHRyaWJ1dGVzIg0xMDQ0Mzc3Mwc6BkVUOhBAYXR0cmlidXRlc3sA--f05c930091e222427cb2cebbe2879ee6849de5a7; locale=en; ga_new_or_upgraded_user_koolestnitz=1; _ga=GA1.2.1681402444.1544348579; _gid=GA1.2.627008198.1544348579; launcher_token=15f9ecc8",
        'cache-control': "no-cache",
        'postman-token': "4e742e6b-efd5-83f5-2b51-6eb7b556868e"
        }
        
        response = requests.request("GET", url, headers=headers)
        
        meetings_recordings = json.loads(response.text)
        #print(meetings_recordings['audio'][0]['download_path'])
        new_meeting_id[id] =  meetings_recordings['audio'][0]['download_path']
        meeting_id[id] =  new_meeting_id[id]
        urllib.request.urlretrieve(new_meeting_id[id],"new-recordings/mp3/"+id+".mp3")
    pickle.dump(meeting_id, open("meeting_id.pkl", "wb"))

def printit():
  threading.Timer(5.0, printit).start()
  getMp3()
  print("Hello, World!")

printit()