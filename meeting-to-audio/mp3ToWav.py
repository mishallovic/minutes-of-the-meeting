# -*- coding: utf-8 -*-
"""
Created on Sun Dec  9 22:20:35 2018

@author: nitesh
"""

from pydub import AudioSegment
sound = AudioSegment.from_mp3("new-recordings/mp3/192802072.mp3")
sound.export("new-recordings/wav/192802072.wav", format="wav")