# -*- coding: utf-8 -*-
"""
Created on Sun Dec  9 20:54:51 2018

@author: nitesh
"""

import speech_recognition as sr 
r = sr.Recognizer() 
with sr.AudioFile("new-recordings/wav/Razer_Blade_tealth_2019-3PtbK_0j1sw-[AudioTrimmer.com].wav") as source:
     audio = r.record(source)
try: 
    text = r.recognize_google_cloud(audio) 
    print ("you said: " + text )
    #error occurs when google could not understand what was said 
except sr.UnknownValueError: 
    print("Google Speech Recognition could not understand audio") 
except sr.RequestError as e: 
    print("Could not request results from Google Speech Recognition service; {0}".format(e)) 